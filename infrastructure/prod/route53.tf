##########################################
# ROUTE 53                               #
##########################################

resource "aws_route53_zone" "private" {
  name = "hooli.ch"

  vpc {
    vpc_id = aws_vpc.main.id
  }
}

resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.private.zone_id
  name    = "www.hooli.ch"
  type    = "A"

  alias {
    name                   = aws_elb.hooli.dns_name
    zone_id                = aws_elb.hooli.zone_id
    evaluate_target_health = true
  }
}